package es.jon.springboottestprimer.checkout.application;

import es.jon.springboottestprimer.checkout.domain.CheckoutProcess;
import es.jon.springboottestprimer.checkout.domain.CheckoutProcessOutput;
import es.jon.springboottestprimer.checkout.domain.Product;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * @author jrodriguez
 */
@AllArgsConstructor
public class CheckoutService {
    private final List<Product> products;
    private final CheckoutProcess checkoutProcess;

    public double checkout() throws Exception {
        CheckoutProcessOutput checkoutProcessOutput = checkoutProcess.priceCalculator(products);
        if (!checkoutProcessOutput.isExecutionOk()) throw new Exception(checkoutProcessOutput.getMessage());

        return checkoutProcessOutput.getPrice();

    }
}

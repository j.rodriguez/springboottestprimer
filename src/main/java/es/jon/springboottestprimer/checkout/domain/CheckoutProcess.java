package es.jon.springboottestprimer.checkout.domain;

import java.util.List;

/**
 * @author jrodriguez
 */
public class CheckoutProcess {

    public CheckoutProcessOutput priceCalculator(List<Product> products) {
        if (products == null) return new CheckoutProcessOutput(false, "Null list of products", 0.0);
        double price = products.stream()
                .map(Product::getPrice)
                .reduce(0.0, Double::sum);
        return new CheckoutProcessOutput(true, "Ok", price);
    }
}

package es.jon.springboottestprimer.checkout.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author jrodriguez
 */
@AllArgsConstructor
@Getter
public class CheckoutProcessOutput {
    private final boolean executionOk;
    private final String message;
    private final double price;
}

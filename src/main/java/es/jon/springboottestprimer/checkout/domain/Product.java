package es.jon.springboottestprimer.checkout.domain;

import lombok.Getter;

/**
 * @author jrodriguez
 */
@Getter
public class Product {
    private final String name;
    private final double price;

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }
}

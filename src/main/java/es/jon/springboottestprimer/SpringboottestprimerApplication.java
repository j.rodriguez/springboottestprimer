package es.jon.springboottestprimer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringboottestprimerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringboottestprimerApplication.class, args);
    }

    public String testFunction() {
        return "A";
    }

}

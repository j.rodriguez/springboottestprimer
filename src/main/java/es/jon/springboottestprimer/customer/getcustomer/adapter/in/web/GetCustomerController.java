package es.jon.springboottestprimer.customer.getcustomer.adapter.in.web;

import es.jon.springboottestprimer.customer.getcustomer.adapter.in.web.dto.CustomerDto;
import es.jon.springboottestprimer.customer.getcustomer.application.port.in.GetCustomersUseCase;
import es.jon.springboottestprimer.customer.getcustomer.dto.CustomerQuery;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/customers")
@AllArgsConstructor
public class GetCustomerController {
    private GetCustomersUseCase getCustomersUseCase;

    @GetMapping(value = "/get")
    public List<CustomerDto> getCustomers() {
        return getCustomersUseCase.getCustomers().stream().map(this::fromQueryToDTO).collect(Collectors.toList());
    }

    private CustomerDto fromQueryToDTO(CustomerQuery cq) {
        return new CustomerDto(cq.getId(), cq.getName(), cq.getLastName(), cq.getEmail());
    }
}

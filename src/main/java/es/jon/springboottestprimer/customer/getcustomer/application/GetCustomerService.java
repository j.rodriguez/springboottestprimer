package es.jon.springboottestprimer.customer.getcustomer.application;

import es.jon.springboottestprimer.customer.getcustomer.application.port.in.GetCustomersUseCase;
import es.jon.springboottestprimer.customer.getcustomer.application.port.out.GetCustomersInterface;
import es.jon.springboottestprimer.customer.getcustomer.dto.CustomerQuery;
import lombok.AllArgsConstructor;

import java.util.List;
@AllArgsConstructor
public class GetCustomerService implements GetCustomersUseCase {
    private GetCustomersInterface getCustomersInterface;

    @Override
    public List<CustomerQuery> getCustomers() {
        return getCustomersInterface.getCustomers();
    }
}

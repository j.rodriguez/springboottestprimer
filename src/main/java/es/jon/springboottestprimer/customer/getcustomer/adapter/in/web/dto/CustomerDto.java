package es.jon.springboottestprimer.customer.getcustomer.adapter.in.web.dto;

import lombok.Getter;

@Getter
public class CustomerDto {
    private final long id;
    private final String name;
    private final String lastName;
    private final String email;

    public CustomerDto(long id, String name, String lastName, String email) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
    }
}

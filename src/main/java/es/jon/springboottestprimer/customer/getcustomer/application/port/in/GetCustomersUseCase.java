package es.jon.springboottestprimer.customer.getcustomer.application.port.in;

import es.jon.springboottestprimer.customer.getcustomer.dto.CustomerQuery;

import java.util.List;

public interface GetCustomersUseCase {
    List<CustomerQuery> getCustomers();
}

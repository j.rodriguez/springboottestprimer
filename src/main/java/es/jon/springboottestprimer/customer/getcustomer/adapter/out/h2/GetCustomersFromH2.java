package es.jon.springboottestprimer.customer.getcustomer.adapter.out.h2;

import es.jon.springboottestprimer.customer.getcustomer.adapter.out.h2.db.Customer;
import es.jon.springboottestprimer.customer.getcustomer.adapter.out.h2.repository.CustomerRepository;
import es.jon.springboottestprimer.customer.getcustomer.application.port.out.GetCustomersInterface;
import es.jon.springboottestprimer.customer.getcustomer.dto.CustomerQuery;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
public class GetCustomersFromH2 implements GetCustomersInterface {
    private CustomerRepository customerRepository;

    @Override
    public List<CustomerQuery> getCustomers() {
        List<Customer> customers = customerRepository.findAll();
        return customers.stream().map(this::transformDBToDTO).collect(Collectors.toList());
    }

    private CustomerQuery transformDBToDTO(Customer customer) {
        return new CustomerQuery(customer.getId(), customer.getName(), customer.getLastName(), customer.getEmail());
    }
}

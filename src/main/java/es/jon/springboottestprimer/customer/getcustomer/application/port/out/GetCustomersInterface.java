package es.jon.springboottestprimer.customer.getcustomer.application.port.out;

import es.jon.springboottestprimer.customer.getcustomer.dto.CustomerQuery;

import java.util.List;

public interface GetCustomersInterface {
    List<CustomerQuery> getCustomers();
}

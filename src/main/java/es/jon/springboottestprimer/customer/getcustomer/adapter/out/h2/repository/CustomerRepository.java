package es.jon.springboottestprimer.customer.getcustomer.adapter.out.h2.repository;

import es.jon.springboottestprimer.customer.getcustomer.adapter.out.h2.db.Customer;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

  @Query(value = "select * from customers where name = 'Jon'", nativeQuery = true)
  List<Customer> findJon();
}

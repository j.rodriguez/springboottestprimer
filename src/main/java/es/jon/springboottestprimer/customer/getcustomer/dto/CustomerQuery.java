package es.jon.springboottestprimer.customer.getcustomer.dto;

import lombok.Getter;

@Getter
public class CustomerQuery {
    private final long id;
    private final String name;
    private final String lastName;
    private final String email;

    public CustomerQuery(long id, String name, String lastName, String email) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
    }
}

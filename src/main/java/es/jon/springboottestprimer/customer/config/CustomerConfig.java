package es.jon.springboottestprimer.customer.config;

import es.jon.springboottestprimer.customer.getcustomer.adapter.out.h2.GetCustomersFromH2;
import es.jon.springboottestprimer.customer.getcustomer.adapter.out.h2.repository.CustomerRepository;
import es.jon.springboottestprimer.customer.getcustomer.application.GetCustomerService;
import es.jon.springboottestprimer.customer.getcustomer.application.port.in.GetCustomersUseCase;
import es.jon.springboottestprimer.customer.getcustomer.application.port.out.GetCustomersInterface;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomerConfig {

    @Bean
    public GetCustomersUseCase getCustomersUseCase(CustomerRepository customerRepository) {
        return new GetCustomerService(getCustomersInterface(customerRepository));
    }

    @Bean
    public GetCustomersInterface getCustomersInterface(CustomerRepository customerRepository) {
        return new GetCustomersFromH2(customerRepository);
    }
}

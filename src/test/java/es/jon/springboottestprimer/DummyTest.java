package es.jon.springboottestprimer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author jrodriguez
 */
public class DummyTest {

    @Test
    public void getting_familiar_to_tests() {
        //Arrange
        String expected = "A";
        SpringboottestprimerApplication sut = new SpringboottestprimerApplication();

        //Act
        String result = sut.testFunction();

        //Assert
        assertEquals(expected, result);
    }
}

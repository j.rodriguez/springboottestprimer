package es.jon.springboottestprimer.customer.getcustomer.adapter.in.web.pact.provider;

import static org.mockito.Mockito.when;

import au.com.dius.pact.provider.junit5.HttpTestTarget;
import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junit5.PactVerificationInvocationContextProvider;
import au.com.dius.pact.provider.junitsupport.Provider;
import au.com.dius.pact.provider.junitsupport.State;
import au.com.dius.pact.provider.junitsupport.loader.PactFolder;
import es.jon.springboottestprimer.config.SecurityConfig;
import es.jon.springboottestprimer.customer.getcustomer.application.port.in.GetCustomersUseCase;
import es.jon.springboottestprimer.customer.getcustomer.dto.CustomerQuery;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Provider("get_customer_provider")
@PactFolder("target/mypacts")
@EnableAutoConfiguration(exclude = {SecurityAutoConfiguration.class})
class GetCustomerControllerProviderPactIT {

  @LocalServerPort
  private int serverPort;
  @MockBean
  private GetCustomersUseCase getCustomersUseCase;
  @MockBean
  private SecurityConfig securityConfig;

  @BeforeEach
  void before(PactVerificationContext context) {
    context.setTarget(new HttpTestTarget("localhost", serverPort, "/primer"));
  }

  @TestTemplate
  @ExtendWith(PactVerificationInvocationContextProvider.class)
  void pactVerificationTestTemplate(PactVerificationContext context) {
    context.verifyInteraction();
  }

  @State("there are customers")
  public void toGetState() {
    List<CustomerQuery> expectedCustomers = new ArrayList<>();
    expectedCustomers.add(new CustomerQuery(1, "Jon", "Dow", "jondow@gmail.com"));
    expectedCustomers.add(new CustomerQuery(2, "Rebeca", "Sin", "rebecasin@gmail.com"));
    expectedCustomers.add(new CustomerQuery(3, "Mikel", "Badosa", "mikelbadosa@gmail.com"));
    when(getCustomersUseCase.getCustomers()).thenReturn(expectedCustomers);
  }
}

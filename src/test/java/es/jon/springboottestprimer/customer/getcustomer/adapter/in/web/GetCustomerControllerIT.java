package es.jon.springboottestprimer.customer.getcustomer.adapter.in.web;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import es.jon.springboottestprimer.customer.getcustomer.application.port.in.GetCustomersUseCase;
import es.jon.springboottestprimer.customer.getcustomer.dto.CustomerQuery;
import java.util.ArrayList;
import java.util.List;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(GetCustomerController.class)
class GetCustomerControllerIT {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private GetCustomersUseCase getCustomersUseCase;

    private List<CustomerQuery> expectedCustomers;

    @BeforeEach
    public void setUp() {
        expectedCustomers = new ArrayList<>();
        expectedCustomers.add(new CustomerQuery(1, "Jon", "Dow", "jondow@gmail.com"));
        expectedCustomers.add(new CustomerQuery(2, "Rebeca", "Sin", "rebecasin@gmail.com"));
        expectedCustomers.add(new CustomerQuery(3, "Mikel", "Badosa", "mikelbadosa@gmail.com"));
    }

    @Test
    void all_customers_are_searched() throws Exception {
        //arrange
        when(getCustomersUseCase.getCustomers()).thenReturn(expectedCustomers);

        //act & assert
        this.mockMvc.perform(get("/customers/get").with(SecurityMockMvcRequestPostProcessors.user("jon")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", Matchers.is(1)))
                .andExpect(jsonPath("$[0].name", Matchers.is("Jon")))
                .andExpect(jsonPath("$[0].lastName", Matchers.is("Dow")))
                .andExpect(jsonPath("$[2].email", Matchers.is("mikelbadosa@gmail.com")));
    }
}

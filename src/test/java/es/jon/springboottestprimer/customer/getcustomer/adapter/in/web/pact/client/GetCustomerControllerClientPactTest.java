package es.jon.springboottestprimer.customer.getcustomer.adapter.in.web.pact.client;

import static au.com.dius.pact.consumer.dsl.LambdaDsl.newJsonArrayMinLike;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.core.model.V4Pact;
import au.com.dius.pact.core.model.annotations.Pact;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@ExtendWith(PactConsumerTestExt.class)
//by default the hostInterface will be localhost
@PactTestFor(providerName = "get_customer_provider", hostInterface = "localhost")
class GetCustomerControllerClientPactTest {

  @Pact(provider = "get_customer_provider", consumer = "get_customer_consumer")
  public V4Pact listOfCustomersPact(PactDslWithProvider builder) {
    Map<String, String> headers = new HashMap<>();
    headers.put("Content-Type", "application/json");
    return builder
      .given("there are customers")
      .uponReceiving("a request for customers")
      .path("/customers/get")
      .method("GET")
      .willRespondWith()
      .status(200)
      .headers(headers)
      .body(newJsonArrayMinLike(3, a -> a.object(o -> {
        o.id("id");
        o.stringType("name");
        o.stringType("lastName");
        o.stringType("email");
      })).build())
      .toPact(V4Pact.class);
  }

  @Test
  @PactTestFor
  void getListOfCustomersShouldReturn200(MockServer mockServer) {
    //arrange & act
    ResponseEntity<String> response = new RestTemplate()
      .getForEntity(mockServer.getUrl() + "/customers/get", String.class);

    //assert
    assertThat(response.getStatusCode().value()).isEqualTo(200);
    assertThat(response.getHeaders().get("Content-Type").contains("application/json")).isTrue();
    assertThat(response.getBody()).contains("email", "id", "lastName", "name");
  }
}

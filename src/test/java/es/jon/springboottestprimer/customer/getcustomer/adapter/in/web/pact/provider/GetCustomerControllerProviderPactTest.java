package es.jon.springboottestprimer.customer.getcustomer.adapter.in.web.pact.provider;

import static org.mockito.Mockito.when;

import au.com.dius.pact.provider.junit5.PactVerificationContext;
import au.com.dius.pact.provider.junit5.PactVerificationInvocationContextProvider;
import au.com.dius.pact.provider.junitsupport.Provider;
import au.com.dius.pact.provider.junitsupport.State;
import au.com.dius.pact.provider.junitsupport.loader.PactFolder;
import au.com.dius.pact.provider.spring.junit5.MockMvcTestTarget;
import es.jon.springboottestprimer.customer.getcustomer.adapter.in.web.GetCustomerController;
import es.jon.springboottestprimer.customer.getcustomer.application.port.in.GetCustomersUseCase;
import es.jon.springboottestprimer.customer.getcustomer.dto.CustomerQuery;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

@Provider("get_customer_provider")
@PactFolder("target/mypacts")
class GetCustomerControllerProviderPactTest {

  @Mock
  private GetCustomersUseCase getCustomersUseCase;
  private AutoCloseable closeable;

  @BeforeEach
  void before(PactVerificationContext context) {
    closeable = MockitoAnnotations.openMocks(this);
    MockMvcTestTarget testTarget = new MockMvcTestTarget();
    testTarget.setControllers(new GetCustomerController(getCustomersUseCase));
    context.setTarget(testTarget);
  }

  @AfterEach
  void closeService() throws Exception {
    closeable.close();
  }

  @TestTemplate
  @ExtendWith(PactVerificationInvocationContextProvider.class)
  void pactVerificationTestTemplate(PactVerificationContext context) {
    context.verifyInteraction();
  }

  @State("there are customers")
  public void toGetState() {
    List<CustomerQuery> expectedCustomers = new ArrayList<>();
    expectedCustomers.add(new CustomerQuery(1, "Jon", "Dow", "jondow@gmail.com"));
    expectedCustomers.add(new CustomerQuery(2, "Rebeca", "Sin", "rebecasin@gmail.com"));
    expectedCustomers.add(new CustomerQuery(3, "Mikel", "Badosa", "mikelbadosa@gmail.com"));
    when(getCustomersUseCase.getCustomers()).thenReturn(expectedCustomers);
  }
}

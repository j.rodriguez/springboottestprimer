package es.jon.springboottestprimer.customer.getcustomer.adapter.in.web;

import static org.mockito.Mockito.when;

import es.jon.springboottestprimer.customer.getcustomer.adapter.in.web.dto.CustomerDto;
import es.jon.springboottestprimer.customer.getcustomer.application.port.in.GetCustomersUseCase;
import es.jon.springboottestprimer.customer.getcustomer.dto.CustomerQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class GetCustomerControllerTest {

  @Mock
  private GetCustomersUseCase getCustomersUseCase;

  private AutoCloseable closeable;

  @BeforeEach
  public void setUp() {
    closeable = MockitoAnnotations.openMocks(this);
  }

  @AfterEach
  void closeService() throws Exception {
    closeable.close();
  }

  @Test
  void all_customers_are_searched() {
    //arrange
    List<CustomerQuery> expectedCustomers = new ArrayList<>();
    expectedCustomers.add(new CustomerQuery(1, "Jon", "Dow", "jondow@gmail.com"));
    expectedCustomers.add(new CustomerQuery(2, "Rebeca", "Sin", "rebecasin@gmail.com"));
    expectedCustomers.add(new CustomerQuery(3, "Mikel", "Badosa", "mikelbadosa@gmail.com"));
    when(getCustomersUseCase.getCustomers()).thenReturn(expectedCustomers);
    GetCustomerController sut = new GetCustomerController(getCustomersUseCase);

    //act
    List<CustomerDto> customers = sut.getCustomers();

    //assert
    Map<String, CustomerDto> customersByName = customers.stream().collect(Collectors.toMap(CustomerDto::getName, Function.identity()));
    CustomerDto jon = customersByName.get("Jon");
    Assertions.assertEquals(3, customers.size());
    Assertions.assertEquals("Jon", jon.getName());
    Assertions.assertEquals("Dow", jon.getLastName());
    Assertions.assertEquals("jondow@gmail.com", jon.getEmail());
  }
}

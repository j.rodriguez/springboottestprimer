package es.jon.springboottestprimer.customer.getcustomer.adapter.out.h2.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import es.jon.springboottestprimer.customer.getcustomer.adapter.out.h2.db.Customer;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;

@DataJpaTest
class CustomerRepositoryIT {

  @Autowired
  private DataSource dataSource;
  @Autowired
  private EntityManager entityManager;
  @Autowired
  private CustomerRepository customerRepository;

  @PostConstruct
  void postConstructSetup() {
    customerRepository.deleteAll();
  }

  @AfterEach
  void afterEachSetup() {
    customerRepository.deleteAll();
  }

  @Test
  @Sql(value = "/scripts/sql/createCustomers.sql")
  void there_are_three_customers_in_the_database() {
    assertEquals(3, customerRepository.findAll().size());
    assertNotNull(dataSource);
    assertNotNull(entityManager);
  }

  @Test
  @Sql(value = "/scripts/sql/createCustomers.sql")
  void jons_surname_is_Dow() {
    String expectedLastname = "Dow";

    List<Customer> jonList = customerRepository.findJon();
    Customer jon = jonList.stream().findAny().orElse(null);

    assertNotNull(jon);
    assertEquals(expectedLastname, jon.getLastName());
  }

}

package es.jon.springboottestprimer.customer.getcustomer.application;

import es.jon.springboottestprimer.customer.getcustomer.adapter.out.h2.GetCustomersFromH2;
import es.jon.springboottestprimer.customer.getcustomer.adapter.out.h2.repository.CustomerRepository;
import es.jon.springboottestprimer.customer.getcustomer.dto.CustomerQuery;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

@DataJpaTest(properties = {
        "spring.test.database.replace=NONE"
})
class GetCustomerServiceIT {
    @Autowired
    private CustomerRepository customerRepository;

    @Test
    void test_there_are_three_customers_in_the_database() {
        //arrange
        int expectedCustomers = 3;
        GetCustomerService sut = new GetCustomerService(new GetCustomersFromH2(customerRepository));

        //act
        List<CustomerQuery> customers = sut.getCustomers();

        //assert
        Assertions.assertEquals(expectedCustomers, customers.size());
    }

    @Test
    void test_second_customer_in_the_database_is_rebeca() {
        //arrange
        CustomerQuery expected = new CustomerQuery(1, "Rebeca", "Sin", "rebecasin@gmail.com");
        GetCustomerService sut = new GetCustomerService(new GetCustomersFromH2(customerRepository));

        //act
        List<CustomerQuery> customers = sut.getCustomers();

        //assert
        Assertions.assertEquals(expected.getName(), customers.get(1).getName());
    }
}

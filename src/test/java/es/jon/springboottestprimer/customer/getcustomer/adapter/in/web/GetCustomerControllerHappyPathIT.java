package es.jon.springboottestprimer.customer.getcustomer.adapter.in.web;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.jon.springboottestprimer.customer.getcustomer.adapter.in.web.dto.CustomerDto;
import es.jon.springboottestprimer.customer.getcustomer.dto.CustomerQuery;
import java.util.ArrayList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class GetCustomerControllerHappyPathIT {
    @Autowired
    private TestRestTemplate testRestTemplate;

    private List<CustomerQuery> expectedCustomers;
    private ObjectMapper mapper;

    @BeforeEach
    public void arrange() {
        mapper = new ObjectMapper();
        expectedCustomers = new ArrayList<>();
        expectedCustomers.add(new CustomerQuery(1, "Jon", "Dow", "jondow@gmail.com"));
        expectedCustomers.add(new CustomerQuery(2, "Rebeca", "Sin", "rebecasin@gmail.com"));
        expectedCustomers.add(new CustomerQuery(3, "Mikel", "Badosa", "mikelbadosa@gmail.com"));
    }

    @Test
    void authenticated_users_accessing_customers_are_authorized() throws JsonProcessingException {
        //arrange

        //act
        ResponseEntity<CustomerDto[]> response = testRestTemplate.withBasicAuth("test", "test")
                .getForEntity("/customers/get", CustomerDto[].class);

        //assert
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(mapper.writeValueAsString(response.getBody())).isEqualTo(mapper.writeValueAsString(expectedCustomers));
    }

    @Test
    void unauthenticated_users_accessing_customers_are_not_authorized() {
        //arrange

        //act
        ResponseEntity<String> response = testRestTemplate.getForEntity("/customers/get", String.class);

        //assert
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }
}

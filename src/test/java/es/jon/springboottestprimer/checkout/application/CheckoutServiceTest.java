package es.jon.springboottestprimer.checkout.application;

import es.jon.springboottestprimer.checkout.domain.CheckoutProcess;
import es.jon.springboottestprimer.checkout.domain.CheckoutProcessOutput;
import es.jon.springboottestprimer.checkout.domain.Product;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

/**
 * @author jrodriguez
 */
public class CheckoutServiceTest {

    @Test
    public void checkout_of_products_with_numeric_prices() throws Exception {
        //Arrange
        double expected = 5.0;
        List<Product> products = new ArrayList<>();
        products.add(new Product("test", 2.5));
        products.add(new Product("test", 2.5));
        CheckoutProcess mockedCheckoutProcess = mock(CheckoutProcess.class);
        when(mockedCheckoutProcess.priceCalculator(products)).thenReturn(new CheckoutProcessOutput(true, "Ok", 5.0));
        CheckoutService sut = new CheckoutService(products, mockedCheckoutProcess);

        //Act
        double result = sut.checkout();

        //Assert
        assertEquals(expected, result);
        verify(mockedCheckoutProcess).priceCalculator(products);
    }

    @Test
    public void null_products_throws_exception() {
        //Arrange
        CheckoutProcess mockedCheckoutProcess = mock(CheckoutProcess.class);
        when(mockedCheckoutProcess.priceCalculator(null))
                .thenReturn(new CheckoutProcessOutput(false, "Null list of products", 0.0));

        CheckoutService sut = new CheckoutService(null, mockedCheckoutProcess);

        //Act & Assert
        assertThrows(Exception.class, sut::checkout);
    }
}
